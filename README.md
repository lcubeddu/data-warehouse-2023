PWr 2023-24 Data Warehouse project

This git repo defines a docker container used to deploy our data warehousing architecuture.

The project sources data from a kaggle dataset that registers the top spotify songs everyday, and a scraper that finds additional metadata for these songs.

The project uses Python and Apache Airflow for ETL (to pipeline the data, and schedule retrival), Apache Hadoop for data warehousing, and Tableau for data visualization. Since all of these components require processes to run on a server, the use of a docker container permits us to deploy this environment anywhere, and share it through a git repo.

**THIS CODE IS IN NO WAY SECURE, AND SHOULD NEVER BE USED IN PRODUCTION OR WITH EXPOSED PORTS**

For now, the project is incomplete and only contains :
- Apache Airflow (unconfigured)
- Apache Hadoop HDFS
- Apache Hive
- Apache Spark (By default, 1 master and 2 workers)

# Prerequisites

This project requires GNU/Linux or WSL2, as well as a properly installed docker daemon (that is, with a docker socket accessible to your unprivileged user)
Create the airflow/kaggle.env file like this:

```json
KAGGLE_USERNAME='username'
KAGGLE_KEY='key'
```

# Install 

These commands should be run as a local user (without root access)

Clone the depo

```bash
git clone https://gitlab.com/lcubeddu/data-warehouse-2023.git
cd data-warehouse-2023
```

Download the hadoop ecosystem binaries and copy it into hadoop/tmp_cache using the following command (requires `wget`)

```bash
make download-hadoop-ecosystem
```

Install the docker images

```bash
make clean install
```

# Create/Run containers

```bash
make up
```

# Stop containers

```
make stop
```

# DELETE CONTAINERS

```
make down
```