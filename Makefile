AIRFLOW_PATH = ./airflow
HADOOP_PATH = ./hadoop
SHELL=/bin/bash

SPARK_VERSION = 3.5.0
HADOOP_VERSION = 3.3.6
HIVE_VERSION = 3.1.3

UID := $(shell id -u)

download-spark: # Download Spark into hadoop/tmp_cache
	wget -P $(HADOOP_PATH)/tmp_cache https://dlcdn.apache.org/spark/spark-$(SPARK_VERSION)/spark-$(SPARK_VERSION)-bin-without-hadoop.tgz

download-hadoop: # Download Hadoop into hadoop/tmp_cache
	wget -P $(HADOOP_PATH)/tmp_cache hhttp://archive.apache.org/dist/hadoop/common/hadoop-$(HADOOP_VERSION)/hadoop-$(HADOOP_VERSION).tar.gz

download-hive: # Download Hive into hadoop/tmp_cache
	wget -P $(HADOOP_PATH)/tmp_cache https://archive.apache.org/dist/hive/hive-$(HIVE_VERSION)/apache-hive-$(HIVE_VERSION)-bin.tar.gz

download-hadoop-ecosystem: download-spark download-hadoop download-hive

install-airflow:
	mkdir -p $(AIRFLOW_PATH)/dags $(AIRFLOW_PATH)/logs $(AIRFLOW_PATH)/plugins $(AIRFLOW_PATH)/config
	printf "AIRFLOW_UID=%s\n" "$(UID)" > $(AIRFLOW_PATH)/.env
	printf "AIRFLOW_GID=0\n" >> $(AIRFLOW_PATH)/.env
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml up airflow-init

clean-airflow: down-airflow
	rm -rf $(AIRFLOW_PATH)/logs $(AIRFLOW_PATH)/plugins $(AIRFLOW_PATH)/.env

up-airflow:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml up -d

down-airflow:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml down

stop-airflow:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml stop

install-hadoop:
	docker compose -f $(HADOOP_PATH)/docker-compose.yaml build

up-hadoop:
	docker compose -f $(HADOOP_PATH)/docker-compose.yaml up -d

down-hadoop:
	docker compose -f $(HADOOP_PATH)/docker-compose.yaml down

stop-hadoop:
	docker compose -f $(HADOOP_PATH)/docker-compose.yaml stop

build-hadoop:
	docker compose -f $(HADOOP_PATH)/docker-compose.yaml build

build-airflow:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml build

build: build-airflow build-hadoop

up:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml -f $(HADOOP_PATH)/docker-compose.yaml up -d

stop: stop-airflow stop-hadoop

down:
	docker compose -f $(AIRFLOW_PATH)/docker-compose.yaml -f $(HADOOP_PATH)/docker-compose.yaml down

install: install-airflow install-hadoop

clean: clean-airflow