#!/bin/bash
set -e
sudo service ssh start

# Define cleanup function
cleanup() {
        echo "Container stopped, performing cleanup..."
        $HADOOP_HOME/sbin/stop-dfs.sh
        $HADOOP_HOME/sbin/stop-yarn.sh
        exit 0
}

# Trap SIGTERM
trap 'cleanup' SIGTERM

if [ ! -d "/home/hduser/hdfs/namenode" ]; then
        $HADOOP_HOME/bin/hdfs namenode -format && echo "OK : HDFS namenode format operation finished successfully !"
fi

$HADOOP_HOME/sbin/start-dfs.sh

echo "YARNSTART = $YARNSTART"
if [[ -z $YARNSTART || $YARNSTART -ne 0 ]]; then
        echo "running start-yarn.sh"
        $HADOOP_HOME/sbin/start-yarn.sh
fi

$HADOOP_HOME/bin/hdfs dfs -mkdir -p /tmp
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/hive/warehouse
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/hduser
$HADOOP_HOME/bin/hdfs dfs -chmod g+w /tmp
$HADOOP_HOME/bin/hdfs dfs -chmod g+w /user/hive/warehouse

$HADOOP_HOME/bin/hdfs dfsadmin -safemode leave

# Setup Hive
if [ ! -d "$HIVE_HOME/metastore_db" ]; then
        echo "Setting up Hive metastore..."
        cd $HIVE_HOME
        $HIVE_HOME/bin/schematool -dbType derby -initSchema
        cd
fi

# Start hiveserver2
echo "Starting hiveserver2..."
cd $HIVE_HOME
$HIVE_HOME/bin/hiveserver2 &
cd

# keep the container running indefinitely
tail -f $HADOOP_HOME/logs/hadoop-*-namenode-*.log &

wait $!