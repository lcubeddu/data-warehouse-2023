# run spark test on master localhost:7077

spark-submit --class org.apache.spark.examples.SparkPi --master spark://hadoop:7077 $SPARK_HOME/examples/jars/spark-examples_2.12-3.5.0.jar 10

beeline -u jdbc:hive2://hadoop:10000/default -n hduser -e "select 1*2*3*4*5*6*7*8*9*10"