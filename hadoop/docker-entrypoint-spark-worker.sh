#!/bin/bash
set -e
sudo service ssh start

# Define cleanup function
cleanup() {
        echo "Container stopped, performing cleanup..."
        $SPARK_HOME/sbin/stop-worker.sh
        exit 0
}

# Trap SIGTERM
trap 'cleanup' SIGTERM


# Start spark worker
echo "Starting spark worker..."
$SPARK_HOME/sbin/start-worker.sh $SPARK_MASTER_URL -p $SPARK_WORKER_PORT --webui-port $SPARK_WORKER_WEBUI_PORT

# keep the container running indefinitely
tail -f $SPARK_HOME/logs/spark-*-org.apache.spark.deploy.worker*.out &

wait $!