#!/bin/bash
set -e
sudo service ssh start

# Define cleanup function
cleanup() {
        echo "Container stopped, performing cleanup..."
        $SPARK_HOME/sbin/stop-master.sh
        exit 0
}

# Trap SIGTERM
trap 'cleanup' SIGTERM

# Start spark master
echo "Starting spark master..."
$SPARK_HOME/sbin/start-master.sh -p 7077 --webui-port 8080

# keep the container running indefinitely
tail -f $SPARK_HOME/logs/spark-*-org.apache.spark.deploy.master*.out &

wait $!