from datetime import datetime, timedelta
import pandas as pd
from sqlalchemy import create_engine, text
from airflow import DAG
from airflow.decorators import task
from airflow.operators.python import PythonOperator
import kaggle
import os

def fetch_kaggle_dataset():
    os.makedirs('./data', exist_ok=True)
    kaggle.api.dataset_download_files('asaniczka/top-spotify-songs-in-73-countries-daily-updated',
        path= './data',
        unzip=True)
    
def load_kaggle_dataset(**context):
    home = os.path.expanduser('~')
    songs_df = pd.read_csv('./data/universal_top_spotify_songs.csv')
    print(songs_df.head())
    print(songs_df.dtypes)
    # Output:
    # spotify_id             object
    # name                   object
    # artists                object
    # daily_rank              int64
    # daily_movement          int64
    # weekly_movement         int64
    # country                object
    # snapshot_date          object
    # popularity              int64
    # is_explicit              bool
    # duration_ms             int64
    # album_name             object
    # album_release_date     object
    # danceability          float64
    # energy                float64
    # key                     int64
    # loudness              float64
    # mode                    int64
    # speechiness           float64
    # acousticness          float64
    # instrumentalness      float64
    # liveness              float64
    # valence               float64
    # tempo                 float64
    # time_signature          int64
    
    context['ti'].xcom_push(key='songs_df', value=songs_df)

def save_to_shared_folder(**context):
    songs_df = context['ti'].xcom_pull(key='songs_df')
    # Save to /opt/airflow/dockershare/airflow/
    songs_df.to_csv('/opt/airflow/dockershare/airflow/spotify_top_songs.csv', index=False)

def create_hive_table(**context):
    username = 'hduser'
    hostname = 'host.docker.internal'
    port = 10000
    database = 'default'
    engine = create_engine(f"hive://{username}@{hostname}:{port}/{database}")
    query = text("""
                    DROP TABLE IF EXISTS spotify_top_songs
                    """)
    with engine.connect() as conn:
        conn.execute(query)
        print('Table spotify_top_songs dropped successfully')
    query = text("""
                 CREATE TABLE spotify_top_songs (
                        spotify_id STRING,
                        name STRING,
                        artists STRING,
                        daily_rank INT,
                        daily_movement INT,
                        weekly_movement INT,
                        country STRING,
                        snapshot_date STRING,
                        popularity INT,
                        is_explicit BOOLEAN,
                        duration_ms INT,
                        album_name STRING,
                        album_release_date STRING,
                        danceability FLOAT,
                        energy FLOAT,
                        key INT,
                        loudness FLOAT,
                        mode INT,
                        speechiness FLOAT,
                        acousticness FLOAT,
                        instrumentalness FLOAT,
                        liveness FLOAT,
                        valence FLOAT,
                        tempo FLOAT,
                        time_signature INT
                    )
                 ROW FORMAT DELIMITED
                 FIELDS TERMINATED BY ','
                    """)
    with engine.connect() as conn:
        conn.execute(query)
        print('Table spotify_top_songs created successfully')

def hive_local_data_import():
    username = 'hduser'
    hostname = 'host.docker.internal'
    port = 10000
    database = 'default'
    engine = create_engine(f"hive://{username}@{hostname}:{port}/{database}")
    query = text("""
                 LOAD DATA LOCAL INPATH '/home/hduser/dockershare/airflow/spotify_top_songs.csv' 
                 OVERWRITE INTO TABLE spotify_top_songs
                 """)
    with engine.connect() as conn:
        conn.execute(query)
        print('Data imported successfully')

                
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2024, 1, 1) + timedelta(hours=6),
    'schedule_interval': '@daily'
}

with DAG('fetch_kaggle_dataset_dag', default_args=default_args) as dag:
    op_fetch_kaggle_dataset = PythonOperator(
        task_id='fetch_kaggle_dataset',
        python_callable=fetch_kaggle_dataset
    )

    op_load_kaggle_dataset = PythonOperator(
        task_id='load_kaggle_dataset',
        python_callable=load_kaggle_dataset,
        provide_context=True
    )

    op_save_to_shared_folder = PythonOperator(
        task_id='save_to_shared_folder',
        python_callable=save_to_shared_folder,
        provide_context=True
    )

    op_create_hive_table = PythonOperator(
        task_id='create_hive_table',
        python_callable=create_hive_table,
        provide_context=True
    )

    op_hive_local_data_import = PythonOperator(
        task_id='hive_local_data_import',
        python_callable=hive_local_data_import
    )

    op_fetch_kaggle_dataset >> op_load_kaggle_dataset >> op_save_to_shared_folder >> op_create_hive_table >> op_hive_local_data_import
